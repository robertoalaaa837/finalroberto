
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  import { getDatabase, onValue, ref as refS, set, child, get, update, remove, onChildAdded, onChildChanged, onChildRemoved } from
  "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

   //storage

  import { getStorage, ref, uploadBytesResumable, getDownloadURL }
  from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";


  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyA3CtqcWk6u6SbPdYieEcuSzVNnIo2Jml4",
    authDomain: "proyectomuweb.firebaseapp.com",
    databaseURL: "https://proyectomuweb-default-rtdb.firebaseio.com",
    projectId: "proyectomuweb",
    storageBucket: "proyectomuweb.appspot.com",
    messagingSenderId: "100106248970",
    appId: "1:100106248970:web:d049f1ff1760e37d1e90f3"
  };



// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

// Listar Productos
Listarproductos();

function Listarproductos() {
    const section = document.getElementById("sectProductos");

    const dbref = refS(db, 'Productos');

    onValue(dbref, (snapshot) => {
        const productosPorCategoria = {};

        snapshot.forEach(childSnapshot => {
            const data = childSnapshot.val();
            const categoria = data.categoria;

            if (!productosPorCategoria[categoria]) {
                productosPorCategoria[categoria] = [];
            }
            productosPorCategoria[categoria].push(data);
        });

        section.innerHTML = '';

        for (const [categoria, productos] of Object.entries(productosPorCategoria)) {
            section.innerHTML += `<br><hr><h2>${categoria}</h2>`;

            productos.forEach(producto => {
                section.innerHTML += `<div class='card'>
                    <img class='imagen id='urlImag' src='${producto.urlImag}' alt='' width='100%'>
                    <p id='marca' class='anta-regurar'>${producto.marca}</p>
                    <p id='descripcion'>${producto.descripcion}</p>
                    <button>Guardar</button>
                </div>`;
            });
        }
    }, { onlyOnce: true });
}

      