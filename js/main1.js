// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase, onValue, ref as refS, set, child, get, update, remove, onChildAdded, onChildChanged, onChildRemoved } from
"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

 //storage

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";


// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA3CtqcWk6u6SbPdYieEcuSzVNnIo2Jml4",
  authDomain: "proyectomuweb.firebaseapp.com",
  databaseURL: "https://proyectomuweb-default-rtdb.firebaseio.com",
  projectId: "proyectomuweb",
  storageBucket: "proyectomuweb.appspot.com",
  messagingSenderId: "100106248970",
  appId: "1:100106248970:web:d049f1ff1760e37d1e90f3"
};



// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

// Variables de la imagen
const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');


//Funciones
function leerInputs() {
    const numCodigo = document.getElementById('txtCodigo').value;
    const marca = document.getElementById('txtMarca').value;
    const categoria = document.getElementById('txtCategoria').value;
    const descripcion = document.getElementById('txtDescripcion').value;
    const urlImag = document.getElementById('txtUrl').value;

    return { numCodigo, marca, categoria, descripcion, urlImag };
}

function mostrarMensaje(mensaje) {
    const mensajeElement = document.getElementById('mensaje');
    mensajeElement.textContent = mensaje;
    mensajeElement.style.display = 'block';
    setTimeout(() => { mensajeElement.style.display = 'none' }, 1000);
}

//Agregar productos a la DB
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);

function insertarProducto() {
    const { numCodigo, marca, categoria, descripcion, urlImag } = leerInputs();
    //validar
    if (numCodigo === "" || marca === "" || categoria === "" || descripcion === "") {
        mostrarMensaje("Faltaron Datos por Capturar");
        return;
    }
    set(
        refS(db, 'Productos/' + numCodigo),
        {
            numCodigo: numCodigo,
            marca: marca,
            categoria: categoria,
            descripcion: descripcion,
            urlImag: urlImag
        }
    ).then(() => {
        alert("Se agrego con exito");
        limpiarInputs();
    }).catch((error) => {
        alert("Ocurrio un error :(")
    })
}

function limpiarInputs() {
    document.getElementById('txtCodigo').value = '';
    document.getElementById('txtCategoria').value = '';
    document.getElementById('txtMarca').value = '';
    document.getElementById('txtDescripcion').value = '';
    document.getElementById('txtUrl').value = '';
}

function escribirInputs(numCodigo, marca, categoria, descripcion, urlImag) {
    document.getElementById('txtCodigo').value = numCodigo;
    document.getElementById('txtCategoria').value = categoria;
    document.getElementById('txtMarca').value = marca;
    document.getElementById('txtDescripcion').value = descripcion;
    document.getElementById('txtUrl').value = urlImag;
}

function buscarProducto() {
    const numCodigo = document.getElementById('txtCodigo').value.trim();
    if (numCodigo === "") {
        mostrarMensaje("No se ingreso un num de Codigo");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Productos/' + numCodigo)).then((snapshot) => {
        if (snapshot.exists()) {
            const { marca, categoria, descripcion, urlImag } = snapshot.val();
            escribirInputs(numCodigo, marca, categoria, descripcion, urlImag);
        } else {
            limpiarInputs();
            mostrarMensaje("El producto con codigo " + numCodigo + " No Existe:(");
        }
    })
}

const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', buscarProducto);

//Listar Productos

function Listarproductos() {
    const dbref = refS(db, 'Productos');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';

    onChildAdded(dbref, (snapshot) => {
        const childKey = snapshot.key;
        const data = snapshot.val();
        agregarFila(tabla, childKey, data);
    });

    onChildChanged(dbref, (snapshot) => {
        const childKey = snapshot.key;
        const data = snapshot.val();
        actualizarFila(tabla, childKey, data);
    });

    onChildRemoved(dbref, (snapshot) => {
        const childKey = snapshot.key;
        eliminarFila(tabla, childKey);
    });
}

function agregarFila(tabla, key, data) {
    const tbody = tabla.querySelector('tbody');
    var fila = document.createElement('tr');
    fila.id = key;

    var celdaCodigo = document.createElement('td');
    celdaCodigo.textContent = key;
    fila.appendChild(celdaCodigo);

    var celdaNombre = document.createElement('td');
    celdaNombre.textContent = data.marca;
    fila.appendChild(celdaNombre);

    var celdaPrecio = document.createElement('td');
    celdaPrecio.textContent = data.categoria;
    fila.appendChild(celdaPrecio);

    var celdaCantidad = document.createElement('td');
    celdaCantidad.textContent = data.descripcion;
    fila.appendChild(celdaCantidad);

    var celdaImagen = document.createElement('td');
    var imagen = document.createElement('img');
    imagen.src = data.urlImag;
    imagen.width = 100;
    celdaImagen.appendChild(imagen);
    fila.appendChild(celdaImagen);
    tbody.appendChild(fila);
}

function actualizarFila(tabla, key, data) {
    const fila = document.getElementById(key);
    if (fila) {
        fila.cells[1].textContent = data.marca;
        fila.cells[2].textContent = data.categoria;
        fila.cells[3].textContent = data.descripcion;
        fila.cells[4].firstChild.src = data.urlImag;
    }
}

function eliminarFila(tabla, key) {
    const fila = document.getElementById(key);
    if (fila) {
        fila.remove();
    }
}

Listarproductos();

//Funcion actualizar

function actualizarProducto() {
    const { numCodigo, marca, categoria, descripcion, urlImag } = leerInputs();
    if (numCodigo === "" || marca === "" || categoria === "" || descripcion === "") {
        mostrarMensaje("Favor de capturar toda la informacion");
        return;
    }
    
    update(refS(db, 'Productos/' + numCodigo), {
        numCodigo: numCodigo,
        marca: marca,
        categoria: categoria,
        descripcion: descripcion,
        urlImag: urlImag
    }).then(() => {
        mostrarMensaje("Se actualizo con exito");
        limpiarInputs();
    }).catch((error) => {
        mostrarMensaje("Ocurrio un error: " + error);
    });
}

const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarProducto);

//Funcion Borrar

function eliminarProducto() {
    const numCodigo = document.getElementById('txtCodigo').value.trim();
    if (numCodigo === "") {
        mostrarMensaje("No se ingreso un Codigo Valido.");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Productos/' + numCodigo)).then((snapshot) => {
        if (snapshot.exists()) {
            remove(refS(db, 'Productos/' + numCodigo)).then(() => {
                mostrarMensaje("Producto eliminado con éxito.");
                limpiarInputs();
            }).catch((error) => {
                mostrarMensaje("Ocurrio un error al eliminar el producto: " + error);
            });
        } else {
            limpiarInputs();
            mostrarMensaje("El producto con ID " + numCodigo + " no existe.");
        }
    });
}

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarProducto);


// Subir imagen y obtener URL
uploadButton.addEventListener('click',  (event) => {
    event.preventDefault();
    const file = imageInput.files[0];

    if(file){
        const storageRef = ref(storage, file.name);
        const uploadTask = uploadBytesResumable(storageRef, file);
        uploadTask.on('state_changed', (snapshot) =>{
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            progressDiv.textContent = 'Progreso: ' + progress.toFixed(2) + '%';
        }, (error) => {
            console.error(error);
        }, () => {
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                txtUrlInput.value = downloadURL;
                setTimeout(() => {
                    progressDiv.textContent = '';

                }, 500);
            }).catch((error) =>{
                console.error(error);
            });
        });

    }
});